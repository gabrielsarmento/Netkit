sudo rm -rf /opt/netkit

sudo apt-get install libc6:i386 libncurses5:i386 libreadline6:i386

wget -c http://wiki.netkit.org/download/netkit/netkit-2.8.tar.bz2
wget -c http://wiki.netkit.org/download/netkit-filesystem/netkit-filesystem-i386-F5.2.tar.bz2
wget -c http://wiki.netkit.org/download/netkit-kernel/netkit-kernel-i386-K2.8.tar.bz2

sudo tar -jxvf netkit-2.8.tar.bz2 -C /opt
sudo tar -jxvf netkit-filesystem-i386-F5.2.tar.bz2 -C /opt
sudo tar -jxvf netkit-kernel-i386-K2.8.tar.bz2 -C /opt

export NETKIT_HOME=/opt/netkit
export PATH=$PATH:/opt/netkit/bin
export MANPATH=$MANPATH:/opt/netkit/man

echo "export NETKIT_HOME=/opt/netkit" >> /etc/bash.bashrc
echo "export PATH=$PATH:/opt/netkit/bin" >> /etc/bash.bashrc
echo "export MANPATH=$MANPATH:/opt/netkit/man" >> /etc/bash.bashrc
echo "PLEASE RESTART YOUR SYSTEM"
